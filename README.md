# README #

The task proposed in the challenge is to write a command line tool that takes in a CSV file, overwrites all the data of a given column by a given value, and outputs the results into a new CSV file.
More specifically, this command line tool should accept the following arguments:
the filename of a CSV file,   
   * the name of the column to overwrite in that file,   
   * the string that will be used as a replacement for that column,   
   * the filename where the output will be written.he filename of a CSV file,   
   * the name of the column to overwrite in that file,   
   * the string that will be used as a replacement for that column,   
   * the filename where the output will be written.   
Here is how to deal with edge cases:   
 * if the input file is empty, the program should write “input file missing” to the console.   
 * if the input file does not contain the specified column, the program should write “column name doesn’t exist in the input file” to the console.   

The full details can be found in this [link](www.bfilipek.com/2017/09/the-expressive-cpp17-challenge.html)

### How do I get set up? ###

 * Download the cpp file and compile with your favourite compiler using c++17 mode.   
    For eg. g++ -std=c++1z -O2 -Wall -pedantic -pthread FluentChallenge.cpp -lstdc++fs -o FluentChallenge
 