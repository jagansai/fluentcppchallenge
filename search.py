import os.path
import sys
import re

def contains(text, *pattern):
    for p in pattern:
        if p.lower() in text.lower():
            return True
    return False

def exact_match(text, pattern, case_sensitive):
    result = re.findall(pattern, text, 0 if case_sensitive else re.IGNORECASE)
    return len(result) > 0, result


def get_match_higlights(text, pattern, case_sensitive=True):
    (found, result) = exact_match(text, pattern, case_sensitive)
    if found:
        for item in result:
            text = text.replace(item, '\x1b[0;31;43m {} \x1b[0m'.format(item))
        return True, text
    else:
        return False, ''


def walk_through_files(path, file_extension):
    for dirpath, dnames, fnames in os.walk(path):
        for f in fnames:
            if exclude_path != [] and contains(dirpath, *exclude_path):
                continue
            if file_extension == '' or f.endswith(file_extension):
                yield os.path.join(dirpath, f)


def search_for(file, pattern):
    with open(file, mode='r') as f:
        for line in f:
            (result, modified_line) = get_match_higlights(line, pattern, ignore_case)
            if result:
                if only_file:
                    yield file, None
                    break
                else:
                    yield file, modified_line


only_file = False
exclude_path = []
file_extension = ''
ignore_case = True
debug = False


def print_usage():
    print(
        'search <directory> <pattern to search> -name <file_extension> [-file ( to include only file names in the '
        'results '
        ')] [-exclude (to '
        '-x certain directories\files)] [-cs (case sensitive.Default case insensitive)] [-d (Debug)]')

def print_args():
    print("dir:{}, pattern:{},exclude_dir:{},file_pattern:{},cs:{},debug:{}".format(sys.argv[1], 
        sys.argv[2], exclude_path, file_extension, ignore_case == False, debug))

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print_usage()
        exit(-1)
    for index, args in enumerate(sys.argv):
        if args == '-file':
            only_file = True
        if args == '-x':
            exclude_path = sys.argv[index + 1].split(',')
        if args == '-name':
            file_extension = '' if sys.argv[index + 1] == '*' else sys.argv[index + 1]
        if args == '-cs':
            ignore_case = False
        if args == '-d':
            debug = True
    if debug:
        print_args()

    # print("only_file:{}, exclude_path:{}".format(only_file, exclude_path))
    for file in walk_through_files(sys.argv[1], file_extension):
        for file_name, line in search_for(file, sys.argv[2]):  # pattern to search
            if only_file:
                print(file_name)
            else:
                try:
                    print('{}:{}'.format(file_name, line))
                except IOError:
                    print('error occured in printing')
